#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]){
    int pid1,pid2, estado, n;
    int p1_finalizo = 0, p2_finalizo = 0 ;
    int bandera = 0;
    int i;
    if(argc == 2){
        n = atoi(argv[1]);
    }else{
        printf("Ejecutar como ./procesos <n>\nDonde n es un numero mayor a 5.\n"); exit(1);
    }
    if(n<=5){
        fprintf(stderr,"n tiene que ser mayor a 5\n");
        exit(1);
    }
    printf("n = %d\n",n);
    pid1 = fork();
    if(pid1 == 0){ //Hijo 1
        for(i=0;i<=n;i++){
            printf("[%d]\n",i);
            if(i==5){
                pid2 = fork();
                if(pid2 == 0){ //Hijo 2
                    int i;
                    for(i=100;i<=(95+n);i++){
                        sleep(1);
                        printf("<%d>\n",i);
                    }
                    printf("Proceso 2 finalizado\n");
                    exit(0);
                }
            }           
            sleep(1);
        }
        printf("Proceso 1 finalizado\n");
        exit(0);
    }
    if((pid1>0)&&(pid2>0)) printf("Error al crear los procesos\n");
    while((!p1_finalizo)||(!p2_finalizo)){
        int pid;
        pid = wait(&estado);
        if(pid == pid1) p1_finalizo = 1;
        if(pid == pid2) p2_finalizo = 1;
    }
    printf("Todos los procesos terminaron.\n");
    return 0;
}