#include <pthread.h> //Para el manejo de hilos
#include <stdlib.h>
#include <stdio.h>

//Funcion que ejecutará el hilo
void *func_thread(void* args){
    int *lim = (int*)args;
    int counter;
    //Contando desde 100
    for(counter=0;counter<=*lim; counter++){
        printf("%d\n",100+counter);
        usleep(100);
    }
}

//Funcion principal
int main(int argc, char **argv){
    pthread_t idHilo;
    int lim;
   	int counter=0;
    if(argc<2){
        fprintf(stderr, "Falta el argumento\n");
        exit(-1);
    }
    //Tomando el limite de la linea de comandos
    lim=atoi(argv[1]);
    //Creando el hilo y verificando si hubo un error
    if(pthread_create(&idHilo,NULL,func_thread,&lim)){
        perror("Error");
        exit(-1);
    }
    //Imprimiendo la cuenta principal
    while(counter<=lim){
	    printf("%d\n", counter++);
        usleep(100);
	}
    //Esperando a que termine el hilo
 	pthread_join(idHilo,NULL);
    return 0;
}