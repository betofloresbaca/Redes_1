/* 
----------Servidor UDP------------
gcc srvUDP1.c -o srvUDP1
./srvUDP1
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define MYPORT 4950 /* puerto donde
los cliente envian los paquetes */ 

#define MAXBUFLEN 256 /* Max. cantidad de bytes que podra recibir en una llamada a recvfrom() */ 
#define MAXDOWNLOADS 1000 /* Max. cantidad de descargas simultaneas. */
#define MAXDATALEN 512 //Longitud maxima de datos utiles a enviar en un solo paquete
  
typedef struct segment SEGMENT;

struct segment{
    uint32_t segNum;
    uint16_t control;
    uint16_t dataLen;
    char data[MAXDATALEN];
};

struct parametros{
    char nombre[25];
    struct sockaddr_in their_addr;
    int delayMilis;
    int sockfd;
};

//Funcion que ejecutará el hilo
void enviar_archivo(void *param){    
    //Tomando los parametros
    char fileName[25];
    strcpy(fileName,((struct parametros*)param)->nombre);
    struct sockaddr_in their_addr = ((struct parametros*)param)->their_addr;
    //Otras variables necesarias
    int numbytes; //Num de bytes enviados
    int numRBytes; //Num de bytes enviados
    int i = 0; //Numero de secuencia del paquete a enviar
    char *datas = NULL; //Datos enviados
    int dataslen = 0; //Longitud de los datos
    int delayMilis = ((struct parametros*)param)->delayMilis;
    int sockfd = ((struct parametros*)param)->sockfd; //El nuevo socket para ese flujo
    //struct sockaddr_in my_addr;  /* direccion IP y numero de puerto local */ 
    //Hay que abrir y envíar línea por línea el contenido del archivo de nombre param1
    FILE *archivo;
    SEGMENT seg;
    if((archivo = fopen(fileName,"rb"))== NULL){
        // Si no se pudo abrir el archivo --> Se avisa al Cte (control=2)
        printf("\n\tError: no se pudo abrir el archivo: %s. \n",fileName);
        //seg.data[0]=0;
        seg.segNum=0;
        seg.control=htons(2);
        seg.dataLen=0;
        printf("\n\tEnviando Indicador de archivo no encontrado (control):[%d]\n",ntohs(seg.control)); 
        datas=(char*)&seg;
        dataslen=sizeof(SEGMENT)-sizeof(seg.data);
        if((numbytes=sendto(sockfd, datas, dataslen, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1){
            perror("\n\tError al enviar indicador de archivo no encontrado: sendto\n"); 
            exit(1);
        }
    }else{
        // Si se pudo abrir el archivo se comienza a enviar
        printf("\n\tArchivo %s abierto y listo para enviar...\n",fileName);   
        while(numRBytes = fread(seg.data,sizeof(char),MAXDATALEN,archivo)){
            seg.segNum=i;
            seg.control=0;
            seg.dataLen=numRBytes;
            printf("\n\tEnviando segmento:[%d], Control [%d], \n\t[%d] Bytes",seg.segNum,seg.control,seg.dataLen);
            seg.segNum=htonl(seg.segNum);
            seg.control=htons(seg.control);
            seg.dataLen=htons(seg.dataLen);
            datas=(char*)&seg;
            dataslen=sizeof(SEGMENT)-sizeof(seg.data)+numRBytes;
            if((numbytes=sendto(sockfd, datas, dataslen, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1){
                perror("\n\tError al enviar archivo: sendto\n"); 
                exit(1);
            }
            i++;
            usleep(delayMilis*1000);
        }
        //Contruyendo el indicador de fin de archivo
        //seg.data[0]=0;
        seg.control=htons(1);
        seg.segNum=0;
        seg.dataLen=0;
        printf("\n\tEnviando indicador de fin del archivo (control):[%d]\n",ntohs(seg.control));
        datas=(char*)&seg;
        dataslen=sizeof(SEGMENT)-sizeof(seg.data);
        if((numbytes=sendto(sockfd, datas, dataslen, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1){
            perror("\n\tError al enviar indicador de fin de archivo: sendto\n"); 
            exit(1);
        }
        fclose(archivo);
    }
}

int main(int argc, char *argv[]) {
    int sockfd;
    unsigned short myport = MYPORT;   /* El puerto a utilizar */  
    struct sockaddr_in my_addr;   /* direccion IP y numero de puerto local */ 
    struct sockaddr_in their_addr;    /* direccion IP y numero de puerto del cliente */ 
    /* addr_len contendra el tamanio de la estructura sockadd_in
    y numbytes el numero de bytes recibidos */ 
    int addr_len, numbytes; 
    char buf[MAXBUFLEN];  /* Buffer de recepcion */ 
    char msg[MAXBUFLEN];  /* Buffer para enviar el echo */
    char buffer[MAXBUFLEN];
    pthread_t idsHilos[MAXDOWNLOADS];
    int indThread = 0;
    // Para manejo de comandos
    char comando[25];
    char param1[25];
    char delay[25];
    /* Tratamiento de la linea de comandos. */
    if(argc >= 2){
        /* Se especificó el puerto a escuchar. */
        myport=(unsigned short) atoi(argv[1]);
    }
    /* se crea el socket */ 
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){ 
        perror("Error:");
        exit(1);
    } 

    /* Se establece la estructura my_addr para luego llamar a bind() */ 
    my_addr.sin_family = AF_INET;  /* host byte order */ 
    my_addr.sin_port = htons(myport); /* network byte order (BigEndian)*/ 
    my_addr.sin_addr.s_addr = INADDR_ANY; /* se asigna automaticamente la direccion IP local */ 
    bzero(&(my_addr.sin_zero), 8);  /* rellena con ceros el resto de la estructura */
    /* Se le da un nombre al socket */
    if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1){
        perror("Error:"); 
        exit(1);
    }
    printf("Servidor a la escucha por el puerto: %d\n",myport);
    /* Se reciben los datos */ 
    addr_len = sizeof(struct sockaddr);
    do{
        if((numbytes=recvfrom(sockfd, buf, MAXBUFLEN, 0, (struct sockaddr *)&their_addr, &addr_len)) == -1){
            perror("Error:"); 
            exit(1);
        }
        /* Se visualiza lo recibido */ 
        buf[numbytes] = '\0'; 
        printf("\nMensaje Recibido: [%s]",buf);
        printf("\n\tMensaje proveniente de : %s:%hu",inet_ntoa(their_addr.sin_addr), ntohs(their_addr.sin_port));
        printf("\n\tLongitud del paquete en bytes : %d\n",numbytes);
        //Primero borramos el contenido anterior de los parametros
        memset(comando, 0, sizeof(char)*25);
        memset(param1, 0, sizeof(char)*25);
        memset(delay, 0, sizeof(char)*25);
        //Que contiene buf??
        sscanf(buf,"%s %s %s",comando, param1, delay);  // Tiene forma de comando?
        if(strcmp(comando,"DOWNLOAD")==0){
            //Si lo que se quiere es descargar el archivo
            struct parametros param;
            strcpy(param.nombre,param1);
            param.their_addr = their_addr;
            param.delayMilis = atoi(delay);
            param.sockfd = sockfd;
            printf("\n\tComando Detectado:%s Parámetro:%s",comando, param1);
            //Creando el hilo si no se ha alcanzado el maximo numero de descargas
            if(indThread<MAXDOWNLOADS){
                if(pthread_create(&idsHilos[indThread],NULL,(void *)enviar_archivo,(void *)&param)){
                    perror("Error:");
                    exit(-1);
                }
                usleep(1000);
                indThread++;
            }else{
                fprintf(stderr,"Error: Máximo número de descargas por sesion alcanzado.\n");
            }
        }else{ // No es un comando válido, actua como servidor de echo.   
            sprintf(buffer,"Echo -> IP:%s Puerto:%d MSG:%s", inet_ntoa(their_addr.sin_addr), ntohs(their_addr.sin_port), buf);
            if((numbytes=sendto(sockfd, buffer, strlen(buffer), 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1){
                perror("Error:"); 
                exit(1);
            }
            printf("\tEnviados: %d bytes:--%s--\n",numbytes, buffer);
        }
    }while(strcmp(buf,"SHUTDOWN"));
    /* devolvemos recursos al sistema */ 
    //Aqui va el ciclo para esperar los hilos
    //pthread_join(idHilo,NULL); 
    //Esperar todas las descargas pendientes
    indThread--;
    while(indThread>=0){
        printf("Esperando al Hilo[%d]=%ld\n",indThread,idsHilos[indThread]);
        pthread_join(idsHilos[indThread--],NULL);
    }
    close(sockfd);    
    return 0;
}