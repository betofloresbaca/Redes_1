#ifndef DEFS_H
#define DEFS_H

#include <stdint.h>

/* Se usa para hacer typedefs para acortar nombres y definir estructuras utilies*/

#pragma pack(1) //Para empaquetado de 1 byte

#define MAX_INT_R_MEM 50 //Numero maximo de intentos para reservar memoria
#define MAX_USER_INPUT 100 //Maxima entrada de usuario por consola
#define MAX_SEGMENT_DATA_LEN 1024
//Enumeracion para balores booleanos
enum bool{false,true};
//Typedefs de tipos basicos
typedef uint8_t byte; //Un byte de informacion
typedef uint16_t u_short;
typedef enum bool bool;
typedef char * string; //Una cadena de caracteres
//Typedefs de structuras
typedef struct ND ND;
typedef struct LDL LDL;
//Para las estructuras de arreglos
typedef struct Array Array;
typedef struct Array_int Array_int;
typedef struct Array_byte Array_byte;

//Generica
struct Array{
	unsigned int len;
	void *ptr;
};

//Guarda un arreglo con unsigned con su longitud
struct Array_int{
	unsigned int len;
	int *ptr;
};

//Guarda un arreglo byte con su longitud
struct Array_byte{
	unsigned len;
	byte *ptr;
};

/*  Estructuras para una lista doblemente ligada */
struct LDL{
	unsigned long len;
	ND *begin;
	ND *end;
	ND *current;
};

struct ND{
	void *ptr;
	ND *next;
	ND *prev;
};

#endif