#ifndef STRUCTS_H
#define STRUCTS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "defs.h"

///////////////////////// VERIFY ///////////////////////////
bool check_pointer(const void *);//Verifica un apuntador
bool check_Array(const Array*);

//////////////////// ARCHIVOS ////////////////////////////
FILE *open_file(const string, const string);
bool close_file(FILE **);
bool exist_file(string);

///////////////////////// NEW ////////////////////////////
void *new_memory(size_t,size_t);
void *new_memory_copy(const void *,size_t,size_t);
Array *new_Array(size_t,size_t);
Array *new_Array_copy(const Array*,size_t);
string new_string_copy(const string);
ND *new_ND(void *);

///////////////////////// RESET /////////////////////////////
bool reset_memory(void *, size_t, size_t);
bool reset_Array(Array *,size_t);

///////////////////////// PRINT /////////////////////////////
bool print_array_int(const int *, size_t);
bool print_array_byte(const byte *, size_t);
bool print_Array_int(const Array_int *);
bool print_Array_byte(const Array_byte *);

///////////////////////// FREE //////////////////////////////
bool free_memory(void **);
bool free_Array(Array **);

////////////////////////// OTRAS /////////////////////
string concat_string(string, string);

#endif