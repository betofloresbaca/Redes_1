#ifndef NETUTILS_H
#define NETUTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "defs.h"
#include "structs.h"
#include "basics.h"

typedef struct sockaddr Address;
typedef struct sockaddr_in Address_in;
typedef struct Segment Segment;

struct Segment{
    uint32_t num; //Numero de secuencia
    byte type;    //Tipo
    byte subtype; //Subtipo
    uint32_t extra; //For extra data
    uint16_t len; //Longitud de los datos
    byte data[MAX_SEGMENT_DATA_LEN]; //Datos
};
/* En Segment los tipos son:
     0  Datos binarios
     1  Datos texto
     2  Segmento de control (Sin contenido)
          0  ACK
          1  NACK
          2  Fin de archivo
          3  Error con archivo en el servidor    
 */

int new_UDP_socket(void);
Address_in *new_Address_in_UDP(unsigned long, unsigned short);
bool bind_socket(int, Address_in *);
int send_buffer(int, Address_in *, byte *, size_t);
int recv_buffer(int, Address_in *, byte *, size_t);
bool fill_text_segment(Segment *, uint32_t, string);

#endif