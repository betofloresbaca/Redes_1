/* 
----------Cliente UDP------------
*/ 

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define MYPORT 4951    /* el puerto donde se enviaran los datos por defecto*/ 
#define THEIRPORT 4950 /*El puerto por defecto en el que escucha el servidor*/
#define MAXBUFLEN 600 /* Max. cantidad de bytes que podra recibir en una llamada a recvfrom() */

typedef struct segment SEGMENT;

struct segment{
    uint32_t segNum;
    uint16_t control;
    uint16_t dataLen;
    char data[512];
};

int existeArchivo(char* nombre) {
    FILE* f = NULL;
    f = fopen(nombre,"r");
    if (f == NULL) 
        return 0;
    else {
        fclose(f);
        return 1;
    }
}

int main(int argc, char *argv[]){ 
    int sockfd; //Descriptor del socket
    int cont = 1;
    unsigned short myport=MYPORT, theirport = THEIRPORT;
    char msg[MAXBUFLEN]; //Para mandar los mensajes 
    char * ip; //Ip del servidor
    struct sockaddr_in their_addr, my_addr;
    int  addr_len = sizeof(struct sockaddr), numbytes; //Tamanho de la direccion y el numero de bytes de una envio/recepcion
    char buf[MAXBUFLEN]; //Buffer de recepcion
    // Para manejo de comandos
    char comando[25];
    char param1[25]; //Nombre de archivo en el servidor a descargar
    char finalFN[35];//El nombre final del archivo a crear (finaal File Name)
    FILE *archivo; //Archivo en el que guardar la entrada
    SEGMENT seg; //Para recibir un segmento
    if (argc >= 2){ 
        ip = argv[1]; //Tomando la ip del servidor
        if (argc >= 3){
            theirport = (unsigned short) atoi(argv[2]); //Tomando el puerto del servidor
            if(argc == 4){ //Si se provee el puerto del cliente para enviar
                myport = (unsigned short)atoi(argv[3]); //Tomando el puerto del cliente
            }
        }
    }else{
        fprintf(stderr,"Uso: cteUDP <ip> [puerto]\n");
        exit(1);
    }
    /* Creamos el socket */ 
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("Error al crear socket"); 
        exit(1);
    }
    their_addr.sin_family = AF_INET; /* host byte order - LE*/ 
    their_addr.sin_port = htons(theirport); /* network byte order - BE*/ 
    their_addr.sin_addr.s_addr=inet_addr(ip);
    bzero(&(their_addr.sin_zero), 8); 
    // Se llena la estructura my_addr para luego llamar a bind()
    my_addr.sin_family = AF_INET;  //Familia
    my_addr.sin_port = htons(myport); //Puerto en formato de red
    my_addr.sin_addr.s_addr = INADDR_ANY; //Dir IP local
    bzero(&(my_addr.sin_zero), 8);  //Se rellena con ceros el resto
    //Se le asigna un nombre al socket
    if(bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1){
        perror("bind");
        exit(1);
    }
    //Inicia ciclo de ejecucion de ordenes
    do{
        /* Solicitamos mensaje */
        printf("\nMensaje a enviar: ");
        gets(msg);
        //Primero limpiamos lo que hubiera en los buferes comando y param1
        memset(comando, 0, sizeof(char)*25);
        memset(param1, 0, sizeof(char)*25);
        //Que contiene buf??
        sscanf(msg,"%s %s",comando, param1);  // Tiene forma de comando?
        if(strcmp(comando,"CLOSE")==0) break; //El CLOSE no se envia al servidor
        /* enviamos el mensaje, esta linea contiene una barra invertida al final, indicando que sigue abajo*/ 
        if ((numbytes=sendto(sockfd, msg, strlen(msg), 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1){
            perror("Error al enviar mensaje con: sendto"); 
            exit(1);
        }
        printf("\tEnviados %d bytes hacia %s:%d\n",numbytes,inet_ntoa(their_addr.sin_addr),theirport);
        if(strcmp(comando,"DOWNLOAD")==0 && strlen(param1)>0){
            //Se va a descargar un archivo
            printf("\tComando:%s Parámetro:%s",comando, param1);
            //Creando el nombre del archivo a guardar
            sprintf(finalFN,"%s.copy",param1);
            cont = 0;
            while(existeArchivo(finalFN)){
                sprintf(finalFN,"%s.copy(%d)",param1,cont++);
            }
            if((archivo = fopen(finalFN,"wb"))== NULL){ // No se pudo crear el archivo.
                fprintf(stderr,"\n\tError: no se pudo crear el archivo: %s. \n",param1);   
            }else{  // Archivo creado OK
                printf("\n\tArchivo %s creado y listo para recibir...\n",param1);
                do{
                    //Recibiendo los segmentos
                    if((numbytes=recvfrom(sockfd, buf, MAXBUFLEN, 0, (struct sockaddr *)&their_addr, &addr_len )) == -1){
                        perror("\n\tError: No se pudo realizar la recepción de datos con: recvfrom\n"); 
                        exit(1);
                    }
                    //Decodificando el paquete
                    seg.segNum=ntohl(((SEGMENT *)buf)->segNum);
                    seg.control=ntohs(((SEGMENT *)buf)->control);
                    seg.dataLen=ntohs(((SEGMENT *)buf)->dataLen);
                    //Mostrando informacion del paquete que llegó
                    printf("\n\tSegmento recibido:[%d],Control:[%d]",seg.segNum,seg.control);
                    printf("\n\t[%d]Bytes\n",seg.dataLen);
                    if(seg.control==0){
                        //Si es una parte del archivo
                        fwrite(&(((SEGMENT *)buf)->data),1,seg.dataLen,archivo);
                    }else if(seg.control==2){
                        //Si es el indicador de un error al abrir el archivo en el servidor
                        fprintf(stderr,"\n\tArchivo no encontrado en el servidor...\n");
                        break;
                    }else if(seg.control==1){
                        //Si es el indicador de fin de archivo
                        printf("\n\tFin de archivo recibido en el segmento:[%d]\n",seg.segNum);
                        break;
                    }
                }while (ntohs(seg.control)==0);
            }
            //Cerrando el archivo
            fclose(archivo);
        }else{  // No es un comando válido, simplemente imprime lo que recibe del servidor de echo.
            if((numbytes=recvfrom(sockfd, buf, MAXBUFLEN, 0, (struct sockaddr *)&their_addr, &addr_len)) == -1){
                perror("\n\terror en recvfrom");
                exit(1);
            }
            /* Se visualiza lo recibido */ 
            printf("\n\tPaquete proveniente de : %s\n",inet_ntoa(their_addr.sin_addr));
            printf("\tLongitud del paquete en bytes : %d\n",numbytes);
            buf[numbytes] = '\0';
            printf("\tEl paquete contiene : %s\n",buf);
        }
    }while(strcmp("SHUTDOWN",msg));
    close(sockfd);
    return 0;
}