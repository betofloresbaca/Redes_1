#include "netutils.h"

//Crea un nuevo socket UDP
int new_UDP_socket(void){
    int sockfd = -1;
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        fprintf(stderr,"Error: couldn't create new UDP socket");
        //exit(1);
    }
    return sockfd;
}

//Crea una estructura Address_in con los daos proporcionados
Address_in *new_Address_in_UDP(unsigned long ip, unsigned short port){
    Address_in *addr = new_memory(1,sizeof(Address_in));
    if(addr){
        addr->sin_family = AF_INET; /* host byte order - LE*/ 
        addr->sin_port = htons(port); /* network byte order - BE*/ 
        addr->sin_addr.s_addr=ip;
        reset_memory(&(addr->sin_zero), 1, 8); // CHECAR : Puede fallar
    }
    return addr;
}

//Establece la direccion de un socket
bool bind_socket(int sockfd, Address_in *addr){
    if(bind(sockfd, (Address *)addr, sizeof(Address)) == -1){
        fprintf(stderr, "Error: Couldn't bind socket %d\n",sockfd);
        return false;
    }
    return true;
}

//Envia un buffer, hace manejo de errores
int send_buffer(int sockfd, Address_in *addr, byte *data, size_t len){
    int numbytes = -1;
    if((numbytes=sendto(sockfd, data, len, 0, (Address *)addr, sizeof(Address))) == -1){
        fprintf(stderr, "Error: Couldn't recv buffer\n"); 
        //exit(1);
    }
    return numbytes;
}

//Recibe en un buffer
int recv_buffer(int sockfd, Address_in *addr, byte *data, size_t len){
    int numbytes = -1;
    size_t addrlen = sizeof(Address_in);
    if(check_pointer(addr) && check_pointer(data)){
        reset_memory(data, len, sizeof(byte));
        if((numbytes=recvfrom(sockfd, data, len, 0, (Address *)addr, (socklen_t *)&addrlen)) == -1){
            fprintf(stderr, "Error: Couldn't send buffer\n");
            //exit(1);
        }
    }
    return numbytes;
}

//rellena un segmento de datos
bool fill_text_segment(Segment *seg, uint32_t num, string text){
    if(check_pointer(seg) && check_pointer(text)){
        if(reset_memory(seg, 1, sizeof(Segment))){
            seg->num = num;
            seg->type = 1;
            seg->len = strlen(text);
            memcpy(seg->data, text, seg->len);
            return true;
        }
    }
    return false;
}