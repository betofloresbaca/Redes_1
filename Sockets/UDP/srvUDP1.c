/*
----------Servidor UDP------------
gcc srvUDP1.c -o srvUDP1
./srvUDP1
*/

#include <sys/socket.h> 
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>	

#define MYPORT 4950     // Numero del puerto por defectopor el cual el servidor escuchara
#define MAXBUFLEN 100   // Max. cantidad de bytes que podra recibir en una llamada a recvfrom().
#define MAXMSGLEN 200   // Max. cantidad de bytes para formar el mensaje a contestar.

int main(int argc, char *argv[]){
    int sockfd; //Variable que sirve para definir al Socket.
    unsigned short myport=MYPORT;   //Puerto por donde escuchar
    struct sockaddr_in my_addr;     //Direccion IP y puerto local.
    struct sockaddr_in their_addr;  //Direccion IP y puerto del cliente.
    // Tamaño de sockaddr_in y numero de bytes recibidos
    int addr_len=sizeof(struct sockaddr), numbytes;
    char buf[MAXBUFLEN];    //Variable del Buffer de recepción
    char msg[MAXMSGLEN];    //Mensaje a enviar
   //Si el puerto fue especificado en la linea de comandos .
    if(argc >= 2){
        myport=(unsigned short)atoi(argv[1]);
    }
    //Se crea el socket y en caso de error se termina con 1
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("socket");
        exit(1);
    }
    // Se llena la estructura my_addr para luego llamar a bind()
    my_addr.sin_family = AF_INET; //Familia
    my_addr.sin_port = htons(myport); //Puerto de escucha
    my_addr.sin_addr.s_addr = INADDR_ANY; //Direccion local
    bzero(&(my_addr.sin_zero), 8);  //Se rellena con ceros el resto de la estructura
    //Se le asigna un nombre al socket y en caso de error se termina con 1
    if(bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1){
        perror("bind");
        exit(1);
    }
	//Esta listo el servidor y se comienza a escuchar
    printf("Servidor iniciado, escuchando en el puerto %d\n",myport);
    do{ //Escuchando mensajes hasta que se reciba CLOSE
        if ((numbytes=recvfrom(sockfd, buf, MAXBUFLEN, 0, (struct sockaddr *)&their_addr, &addr_len)) == -1){
            perror("recvfrom");
            exit(1);
		}
        // Se acotan los datos recibidos
        buf[numbytes] = '\0';
		//Limpiando el msg a enviar
		bzero(msg,MAXMSGLEN);
		//Construyendo la respuesta
        sprintf(msg,"Recibi de: %s:%hu\nMSG: %s",
		inet_ntoa(their_addr.sin_addr),
		ntohs(their_addr.sin_port), buf);
		//Imprimiendo la respuesta
		printf("\n%s\n",msg);
		//Haciendo el echo y si hay error se termina con 1
		if((numbytes=sendto(sockfd, msg, strlen(msg), 0, (struct sockaddr *)&their_addr, addr_len)) == -1){
    		perror("sendto");
    		exit(1);
  	    }
    }while(strcmp(buf,"CLOSE")!=0);
    //Cerrando el socket
    close(sockfd);
    return 0;
}
