/*
----------Cliente UDP------------
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MYPORT 4951 //El Puerto del cliente para enviar
#define THEIRPORT 4950 //El puerto del servidor
#define MAXBUFLEN 200 //Max. cantidad de bytes que podra recibir en una llamada a recvfrom()
#define MAXMSGLEN 100 //Max. cantidad de bytes que podra enviar en una llamada a sendto()


int main(int argc, char *argv[]){
    int sockfd;  //Variable que sirve para definir al Socket
    unsigned short theirport=THEIRPORT, myport=MYPORT; //Asignando los valores por defecto a los puertos
    char msg[MAXMSGLEN]; //Variable que guardara el mensaje a enviar
    char * ip; //Variable que sirve para almacenar la dirección de IP
    struct sockaddr_in their_addr, my_addr;// IP y puerto del servidor (their) y la propia (my)
    // Tamaño de sockaddr_in y numero de bytes recibidos
    int addr_len=sizeof(struct sockaddr), numbytes;
    char buf[MAXBUFLEN];     //Buffer de recepción
    int lenMSG; //Longitud del mensaje a enviar
    
	//Si se tienen suficientes parametros
    if(argc >= 2){
        ip = argv[1]; // Asigna a la variable ip el valor del primer argumento
        if(argc >= 3){ //Si se provee el puerto de escucha del servidor
            theirport = (unsigned short)atoi(argv[2]);
			if(argc == 4){ //Si se provee el puerto del cliente para enviar
				myport = (unsigned short)atoi(argv[3]);
			}
        }
    }else{ //En caso que no se tengan suficientes parametros
		   //se muestra el uso y se termina el programa
        fprintf(stderr,"Uso: cteUDP <ipServidor> [puertoServidor] [puertoCliente]\n");
        exit(1);  
    }
    //Se llena la estructura para guardar la direccion del servidor
    their_addr.sin_family = AF_INET;// Familia
    their_addr.sin_port = htons(theirport); // Puerto del servidor en formato de red
    their_addr.sin_addr.s_addr=inet_addr(ip); // Dir IP del servidor
    bzero(&(their_addr.sin_zero), 8);//Rellenar con ceros el resto
    //Se crea el Socket y si no se puede crear se termina con 1
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("socket");
        exit(1);
    }
    // Se llena la estructura my_addr para luego llamar a bind()
    my_addr.sin_family = AF_INET;  //Familia
    my_addr.sin_port = htons(myport); //Puerto en formato de red
    my_addr.sin_addr.s_addr = INADDR_ANY; //Dir IP local
    bzero(&(my_addr.sin_zero), 8);  //Se rellena con ceros el resto
    //Se le asigna un nombre al socket
    if(bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1){
        perror("bind");
        exit(1);
    }
    //Se inicia el ciclo para enviar mensajes hasta enviar CLOSE
    do{
  	    //Se solicita el mensaje a enviar
      	printf("Mensaje a enviar: ");
  	    gets(msg);
  	    //Se intenta enviar el mensaje y si hay error se termina con 1
  	    if((numbytes=sendto(sockfd, msg, strlen(msg), 0, (struct sockaddr *)&their_addr, addr_len)) == -1){
    		perror("sendto");
    		exit(1);
  	    }
  	    //Mostrando la operacio realizada
      	printf("Enviados %d bytes a %s:%d\n",
				numbytes,inet_ntoa(their_addr.sin_addr),theirport);
		//Reciviendo la respuesta
		if ((numbytes=recvfrom(sockfd, buf, MAXBUFLEN, 0, (struct sockaddr *)&their_addr, &addr_len)) == -1){
            perror("recvfrom");
            exit(1);
		}
		// Se acota el buffer
        buf[numbytes] = '\0';
        printf("Respuesta:\n\"\n%s\n\"\n",buf);
    }while(strcmp("CLOSE",msg)!=0);
    //Cerrando el socket
    close(sockfd);
    return 0;
}