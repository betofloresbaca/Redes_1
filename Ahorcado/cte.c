/* 
----------Cliente UDP------------
    compilar con: make
    make clean: limpia los archivos compilados
*/ 

#include "defs.h"
#include "structs.h"
#include "netutils.h"

#define MYPORT 4951     /* el puerto donde se enviaran los datos por defecto*/ 



bool arguments_parse(int argc, char *argv[], unsigned long *ip, u_short  *theirport, u_short  *myport){
    if(check_pointer(argv) && check_pointer(ip) && check_pointer(theirport) && check_pointer(myport)){
        if (argc >= 2){ 
            *ip = inet_addr(argv[1]); //Tomando la ip del servidor
            if (argc >= 3){
                *theirport = (u_short) atoi(argv[2]); //Tomando el puerto del servidor
                if(argc == 4){ //Si se provee el puerto del cliente para enviar
                    *myport = (u_short)atoi(argv[3]); //Tomando el puerto del cliente
                }
            }
            return true;
        }else{
            fprintf(stderr,"Uso: cteUDP <ip> [puerto]\n");
        }
    }
    return false;
}

bool check_segment(int sockfd, Address_in *their_addr, Segment *seg, uint32_t id){
  //cheacamos el crc
  uint32_t oldCRC=0, newCRC=0; 
  oldCRC = seg->crc;
  seg->crc = 0;
  newCRC = crc32((byte*)seg, sizeof(Segment) - MAX_SEGMENT_DATA_LEN + seg->len);
  //si el crc falla, regresamos falso y enviamos un NACK
  if(oldCRC != newCRC){
    send_nack(sockfd, their_addr, id);
    return false;  
  }else
    return true; //si el crc es correcto respondemos con un true
}

bool safe_recv_Segment(int sockfd, Addres_in *ther_addr, Segment *seg){ 
    int numbytes;                                         
    //Mientras el crc no sea correcto
    do{
        //Recibiendo los Segmentos  
        numbytes=recv_buffer(sockfd, their_addr, (byte *)seg, sizeof(Segment));
    }while(!check_segment(sockfd, their_addr, seg));
  
    return true;
}

int main(int argc, char *argv[]){
    //Descriptor del socket
    int sockfd;
    //Puertos
    u_short my_port=MYPORT;
    u_short their_port = SERVER_DEFAULT_PORT;
    //Direcciones
    Address_in *their_addr = NULL;
    Address_in *my_addr = NULL;
    //Ip del servidor
    unsigned long ip = 0;
    //Descriptor de archivos para escribir los datos recibidos
    FILE *fp = NULL;
    //El nombre del archivo en el que escribir los bytes recibidos
    string fileName = NULL;
    //Numero de bytes de un envio o recepcion
    int  numbytes = 0;
    //Para recibir los segmentos
    Segment *seg = NULL;
    //Para manejo de commands
    string command = NULL; //command ejecutado
    string param1 = NULL; //Primer parametro del command
    //Para recibir entrada de usuario
    string inputLine = NULL;
    //El prompt a mostrar
    string prompt = "Mensaje a enviar: ";
    //Contador del numero de archivo, OBSOLETO
    unsigned cont = 0;
    //Numero de secuencia de paquete a enviar
    unsigned sec = 0;
    //identificador del hilo.
    uint32_t id;
    //Si se le debe preguntar al usuario un nuevo nombre de archivo
    bool askNewFileName = false;
    //array de memoria reservadas
    Array_memory *memory = (Array_memory *)new_Array(7,sizeof(void**));
    if(!check_pointer(memory)) exit(1);
    (memory->ptr)[0] = (void **)&seg;
    (memory->ptr)[1] = (void **)&inputLine;
    (memory->ptr)[2] = (void **)&fileName;
    (memory->ptr)[3] = (void **)&command;
    (memory->ptr)[4] = (void **)&param1;
    (memory->ptr)[5] = (void **)&their_addr;
    (memory->ptr)[6] = (void **)&my_addr;

    //Llenando el Array de memoria reservada
    //Tomando los parametros de entrada
    if(!arguments_parse(argc, argv, &ip, &their_port, &my_port)){
        exit(1);
    }
    //Reservando memoria necesaria
    if( !(seg       = new_memory(1, sizeof(Segment)))           ||
        !(inputLine = new_memory(MAX_USER_INPUT, sizeof(char))) ||
        !(fileName  = new_memory(40, sizeof(char)))             ||
        !(command    = new_memory(COMMAND_SIZE, sizeof(char)))    ||
        !(param1    = new_memory(PARAM_SIZE, sizeof(char)))
        ){
        free_Array_memory(&memory);
        exit(1);
    }
    //Creando el canal de comunicacion
    if(  ((sockfd    = new_UDP_socket()) == -1)                 || // Creacion socket 
        !(their_addr = new_Address_in_UDP(ip, their_port))      || //Formando la direccion del servidor
        !(my_addr    = new_Address_in_UDP(INADDR_ANY, my_port)) || //Formando la direccion propia       
        !(bind_socket(sockfd,my_addr))                             //Asignando la direccion al socket
        ){
        free_Array_memory(&memory);
        exit(1);
    }
    
    //Inicia ciclo de ejecucion de ordenes
    while(true){
        //Primero limpiamos lo que hubiera en los buferes de entrada de usuario
        reset_memory(command, COMMAND_SIZE, sizeof(char));
        reset_memory(param1, PARAM_SIZE, sizeof(char));
        /* Solicitamos mensaje */
        printf("\n%s",prompt);
        read_input_to_string(inputLine, MAX_USER_INPUT);
        //Que contiene buf??
        sscanf(inputLine,"%s %s",command, param1);  // Tiene forma de command?
        if(strcmp(command,"CLOSE")==0) break; //El CLOSE no se envia al servidor
        //Formando un segmento de la entrada de usuario
        fill_text_Segment(seg, sec, inputLine);
        // Enviamos el mensaje
        numbytes = sizeof(Segment) - MAX_SEGMENT_DATA_LEN + seg->len;
        if ((numbytes = send_buffer(sockfd, their_addr, (byte *)seg, numbytes)) == -1){
            free_Array_memory(&memory);
            exit(1);
        }
        printf("\tEnviados %d bytes hacia %s:%d\n",numbytes,inet_ntoa(their_addr->sin_addr),their_port);
        //Tomando la accion necesaria
        if(strcmp(command,"DOWNLOAD")==0 && strlen(param1)>0){ //Si se va a descargar un archivo
            //recivir el indice el hilo
            if((numbytes=recv_buffer(sockfd, their_addr, (byte *)seg, sizeof(Segment))) == -1){
                free_Array_memory(&memory);
                exit(1);
            }
            printf("Se recibieron %d bytes\n", numbytes);
            printf("tipo --> %d  subtipo -->  %d  extra --> %d\n", seg->type, seg->subtype, seg->extra);
            if((seg->type == (byte)2) && (seg->subtype == (byte)4)){
                //guardamos el id del hilo;
                id = seg->extra;
                printf("(t=%d  s=%d)El id es %d", seg->type, seg->subtype, id);
            }
            //Se va a descargar un archivo
            printf("\ncommando:%s Parámetro:%s",command, param1);
            //Si ya se tiene el archivo en el disco local
            askNewFileName = exist_file(param1);
            //Creando el nombre del archivo temporal a guardar
            sprintf(fileName,"%s.temp",param1);
            cont = 0;
            while(exist_file(fileName)){
                sprintf(fileName,"%s.temp(%d)",param1,cont++);
            }
            if((fp = open_file(fileName,"wb")) != NULL){ // Se pudo crear el archivo   
                printf("\n\tArchivo temporal %s creado y listo para recibir...\n",param1);
                //Enviamos un mensaje ack para comenzar la tansmision de datos
                send_ack(sockfd, their_addr, id);    
                    
                while(true){
                    //esperando recivir un segmento
                    safe_recv_Segment(sockfd, ther_addr, &seg); // si pasa
                    //Mostrando informacion del paquete que llegó
                    printf("\n\tSegmento recibido:[%d],Type:[%d], Subtype[%d]",seg->num,seg->type, seg->subtype);
                    printf("\n\t[%d]Bytes\n", seg->len);
                    if(seg->type == 0){
                        //Si es una parte del archivo
                        fwrite(&(seg->data), 1, seg->len, fp);   
                        //enviamos un ACK                                        
                        send_ack(sockfd, their_addr, id);  
                      
                    }else if((seg->type == (byte)2) && (seg->subtype == (byte)3)){ 
                        //Si es el indicador de un error al abrir el archivo en el servidor
                        fprintf(stderr,"\n\tArchivo no encontrado en el servidor...\n");
                        //enviamos un ACK
                        send_ack(sockfd, their_addr, id);  
                        break;
                    }else if((seg->type == (byte)2) && (seg->subtype == (byte)2)){
                        //Si es el indicador de fin de archivo
                        printf("\n\tFin de archivo recibido en el Segmento:[%d]\n",seg->num);
                        //enviamos un ACK
                        send_ack(sockfd, their_addr, id);  
                        break;
                    }
                }
                //Cerrando el archivo
                close_file(&fp);
                //Renombrando el archivo si es necesario
                /*Rutina de renombrado*/
                if(askNewFileName){
                    strcpy(inputLine,fileName);
                    do{
                        printf("El archivo %s ya existe, elige otro nombre: ",inputLine);
                        read_input_to_string(inputLine, MAX_USER_INPUT);
                    }while(exist_file(inputLine));
                    //Renombrando el archivo
                    rename(fileName,inputLine);
                }
            }
        }else if(strcmp("SHUTDOWN", command)==0){
            break;
        }else{  // No es un command válido, simplemente imprime lo que recibe del servidor de echo.
            if((numbytes=recv_buffer(sockfd, their_addr, (byte *)seg, sizeof(Segment))) == -1){
                        free_Array_memory(&memory);
                        exit(1);
            }
            /* Se visualiza lo recibido */ 
            printf("\n\tPaquete proveniente de : %s\n",inet_ntoa(their_addr->sin_addr));
            printf("\tLongitud del paquete en bytes : %d\n",numbytes);
            printf("\tEl paquete contiene : %s\n",seg->data);
        }
    }
    close(sockfd);
    free_Array_memory(&memory);
    return 0;
}