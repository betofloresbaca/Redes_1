/* 
----------Servidor UDP------------
    compilar con: make
    make clean: limpia los archivos compilados
*/

#include "defs.h"
#include "structs.h"
#include "netutils.h"
#include <time.h>

#define MAX_THREADS 1024 /* Max. cantidad de descargas simultaneas. */
#define WAIT_TIME 100 /*Tiempo de espera por un ACK o NACK en milis antes de reenviar*/
#define MAX_LEN_WORD 100 //Longitud máxima de una palabra en el archivo a leer
#define MAX_WORDS 1000 //Cantidad máxima de palabras en el servidor
#define NUM_SYMS 30

enum control{NONE, ACK, NACK};


//Para un mejor control
// Al acabar una palabra inicializar las banderas

//lleva control de los elementos ingresados en el vector acertadas
int band_acertadas = 0;
//lleva control de los elementos ingresados en el vector falladas
int band_falladas = 0;

//Estructura para los parametros de cada hilo
typedef struct{
    int sockfd;
    Address_in their_addr;
    int delayMilis;
    char fileName[30];
    int ind; //Indice dentro del arreglo
    enum control com; //Comunicacion entre main y sus hilos hijos
}ThreadParams;

//Estructura para los atributos de un jugador
typedef struct{
    char alias[30];
    Address_in dirIP;
    int puerto;
    int noErrores;
}jugador;

//Arreglo con los apuntadores a los parametros de cada hilo
ThreadParams *threadParam[MAX_THREADS];

//Vector de palabras
char words[MAX_WORDS][MAX_LEN_WORD];
unsigned nWords = 0;

//Vectores de letras
char letras[3][NUM_SYMS]; //letras[0] -> no probadas
                          //letras[1] -> acertadas
                          //letras[2] -> falladas

//Inicializar vectores de letras
void init_letras(void){
    char L;
    //Poniiendo en blancos lo que pudiera tener el arreglo
    reset_memory(letras, 3, sizeof(char)*NUMSYMS);
    //Poniendo las letras de la A a la N en el arreglo
    for(L='A'; L<='N'; L++){
        letras[0][L-'A'] = L;   
    }
    //Poniendo la Ñ
    letras[0][L-'A'] = 'Ñ';
    //Poniendo de la M a la Z
    for(L='M'; L<='Z'; L++){
        letras[0][L-'A'] = L;   
    }
}

//Funcion que quita una letra del vector de letras 
void pop_acertada(char letra){
  int i;
  //quitar letra del vector no provadas
  for(i=0; i<NUM_SYMS; i++){ // Generalizado
    if(letras[0][i] == letra){
        letra[0][i] = 0;
        break;
    }
  }
  //poner letra en el vector acertadas
  letras[1][band_acertadas++] = letra;
}
  
//Pone una letra en el vector de letras falladas 
void push_fail(char letra){
  int i;
  //quitar letra del vector no provadas
  for(i=0; i<NUM_SYMS; i++){ 
    if(letras[0][i] == letra){
        letra[0][i] = 0;
        break;
    }
  }
  //poner en el vector falladas
  letras[2][band_falladas++] = letra;
}

//Abre un archivo y rellena el vector de palabras 
bool read_words_file(string fN){
    unsigned i;
    FILE fp = open_file(fN, "rt");
    if(!fp) return false;
    //Leyendo el numero de palabras
    fscanf(fp,"%u ", &nWords);
    //Leyendo cada palabra
    for(i=0; i<nWords; i++){
        fscanf(fp, "%s", words[i]);
    }
    close_file(&fp);
}

//Obtiene una palabla aleatoria
string select_random_word(void){
  return words[rand()%nWords];
}

//Identifica los comandos de la linea de ordenes
bool arguments_parse(int argc, char *argv[], u_short  *my_port){
    if(check_pointer(argv) && check_pointer(my_port)){
        if(argc >= 2){
            /* Se especificó el puerto a escuchar. */
            *my_port=(unsigned short) atoi(argv[1]);
        }
        return true;
    }
    return false;
}

//Regresa el indice de un lugar disponiible para guardar parametros
//Si no funciona regresa -1
int find_fill_params(void){ 
    register int i = MAX_THREADS;
    //Recorre todo el arreglo de parametros hasta
    //encontrar un ligar que esta siendo ocupado.
    while(--i > -1) if(threadParam[i]) break;
    return i;
}

int find_blank_params(void){ 
    register int i = MAX_THREADS;
    //Recorre todo el arreglo de parametros hasta
    //encontrar un NULL donde guardar nuevos parametros
    while(--i > -1) if(!threadParam[i]) break;
    return i;
}

int main(int argc, char *argv[]) {
    //Socket a usar
    int sockfd;
    //Puertos
    unsigned short my_port = SERVER_DEFAULT_PORT;
    //Direcciones
    Address_in *my_addr = NULL;
    Address_in *their_addr = NULL;
    //Numero de bytes recibidos o enviados 
    int numbytes;
    //Buffer de recepcion
    Segment *seg_in = NULL;
    //Buffer de envio
    Segment *seg_out = NULL;
    // Para manejo de commands
    string command = NULL;
    //Buffer auxiliar para escribir mensajes de echo
    char auxBuffer[MAX_SEGMENT_DATA_LEN];
    //Indice auxiliar para indicar donde guardar los parametros para un hilo
    int indT = 0;
    //Creando el arreglo de memoria
    Array_memory *memory = (Array_memory *)new_Array(5 , sizeof(void**));
    if(!check_pointer(memory)) exit(1);
    (memory->ptr)[0] = (void **)&seg_in;
    (memory->ptr)[1] = (void **)&seg_out;
    (memory->ptr)[2] = (void **)&my_addr;
    (memory->ptr)[3] = (void **)&their_addr;
    (memory->ptr)[4] = (void **)&command;
    /* Tratamiento de la linea de commands. */
    arguments_parse(argc, argv, &my_port);
    //Reservando memoriia necesaria
    if( !(seg_in    = new_memory(1, sizeof(Segment)))           ||
        !(seg_out   = new_memory(1, sizeof(Segment)))           ||
        !(command   = new_memory(COMMAND_SIZE, sizeof(char)))
        ){
        free_Array_memory(&memory);
        exit(1);
    }
    //Creando el canal de comunicacion
    if(  ((sockfd    = new_UDP_socket()) == -1)                       || // Creacion socket 
        !(their_addr = (Address_in*)new_memory(1,sizeof(Address_in))) || //Formando la direccion del servidor
        !(my_addr    = new_Address_in_UDP(INADDR_ANY, my_port))       || //Formando la direccion propia       
        !(bind_socket(sockfd, my_addr))                                   //Asignando la direccion al socket
        ){
        free_Array_memory(&memory);
        exit(1);
    }
    //Se inicializan los conjuntos de letras
    init_letras();
    //Se leen las palabras del archivo
    read_words_file("palabras.txt");
    //Listo para escuchar
    printf("Servidor a la escucha por el puerto: %d\n", my_port);
    //Ciclo para recibir paquetes
    while(true){
        //Se reciben los bytes
        if((numbytes = recv_buffer(sockfd,their_addr, (byte *)seg_in, sizeof(Segment))) == -1){
            free_Array_memory(&memory);
            exit(1);
        }
        //Acotando el texto
        //Mostrando lo recibido
        printf("\nSe recibio un mensaje:");
        printf("\n\tMensaje proveniente de : %s:%hu", inet_ntoa(their_addr->sin_addr), ntohs(their_addr->sin_port));
        printf("\n\tLongitud del paquete en bytes : %d\n", numbytes);
        if((seg_in->type) == 1 ){   //Si se recibio texto
            //Mostramos el texto recibido
            printf("\tContenido: [%s]\n", seg_in->data);
            //Primero borramos el contenido anterior de los parametros recibidos del cliente
            reset_memory(command, COMMAND_SIZE, sizeof(char));
            reset_memory(param1, PARAM_SIZE, sizeof(char));
            reset_memory(delayStr, PARAM_SIZE, sizeof(char));
            //Que contiene el segmento recibido
            sscanf((char *)(seg_in->data),"%s %s %s",command, param1, delayStr);  // Tiene forma de command?
            if((strcmp(command, "DOWNLOAD") == 0)){ 
                //Si lo que se quiere es descargar el archivo
                //Mostramos informacion sobre lo que se va a hacer
                printf("\n\tComando detectado:%s Parámetro:%s",command, param1);
                //Buscamos un indice para guardar los parametros a pasar
                indT = find_blank_params();
                //Si no hay ningun slot disponible
                if(indT==-1){
                    fprintf(stderr,"Error: Máximo número de descargas por sesion alcanzado.\n");
                    continue;
                }
                //Creamos la estructura de los parametros
                threadParam[indT] = (ThreadParams *)new_memory(1, sizeof(ThreadParams));
                //Si no se pueden crear los parametros se sale para recibir otro segmento
                if(!threadParam[indT]) continue;
                //Copiando los parametros a pasar al hilo
                threadParam[indT]->sockfd = sockfd;
                threadParam[indT]->their_addr = *their_addr;
                strcpy(threadParam[indT]->fileName,param1);
                threadParam[indT]->delayMilis = atoi(delayStr);
                threadParam[indT]->ind = indT;
                //Creando el hilo
                if(pthread_create(&tID, NULL, (void *)send_file, (void *)threadParam[indT])){
                    perror("Error:");
                    exit(-1);
                }
            }else if(strcmp(command, "SHUTDOWN") == 0){ //Si se quiere apagar el servidor
                puts("Apagando el servidor");
                break; //Se termina el ciclo
            }else{ // No es un command válido, actua como servidor de echo 
                 //Formando un paquete de texto
                sprintf(auxBuffer,"Echo -> IP:%s Puerto:%d MSG:%s",
                inet_ntoa(their_addr->sin_addr),
                ntohs(their_addr->sin_port),
                seg_in->data);
                fill_text_Segment(seg_out,0,auxBuffer);
                numbytes = sizeof(Segment) - MAX_SEGMENT_DATA_LEN + seg_out->len;
                if((numbytes = send_buffer(sockfd, their_addr, (byte *)seg_out, numbytes)) == -1){
                    free_Array_memory(&memory);
                    exit(1);
                }
                printf("\tEnviados: %d bytes:--%s--\n",numbytes, seg_out->data);
            }
        }else if((seg_in->type) == 2){ //Si es un segmento de control
            if((seg_in->subtype) == 0){ //Si es un ACK
                puts("Se recibio un ACK");
                //Comprovando el indice recibido
                if((seg_in->extra)<MAX_THREADS && threadParam[seg_in->extra]){
                    //Avisando al hilo
                    threadParam[seg_in->extra]->com = ACK;
                    puts("Se aviso al hilo");
                }
            }else if((seg_in->subtype) == 1){ //Si es un NACK
                puts("Se recibio un ACK");
                //Comprovando el indice recibido
                if((seg_in->extra)<MAX_THREADS && threadParam[seg_in->extra]){
                    //Avisando al hilo
                    threadParam[seg_in->extra]->com = NACK;
                    puts("Se aviso al hilo");
                }
            }
        }
    }
    /* devolvemos recursos al sistema */           
    puts("Esperando los hilos de descargas faltantes");
    while(find_fill_params() != -1);
    close(sockfd);
    free_Array_memory(&memory);
    return 0;
}