#include "structs.h"


///////////////////////// VERIFY ///////////////////////////
bool check_pointer_msg(string msg, const void *ptr){
	if(!ptr){
        fprintf(stderr, "Error: %s\n", msg);
        return false;
    }
    return true;
}


//Verifica si un puntero no es nulo
bool check_pointer(const void *ptr){
	return check_pointer_msg("Null pointer", ptr);
}

//Manejo de la estructura Array
bool check_Array(const Array *array){
    return (check_pointer_msg("Array is NULL", array) &&
    		array->len &&
    		check_pointer_msg("Array->ptr is NULL", array->ptr));
}


///////////////////////ARCHIVOS //////////////////////////


FILE *open_file(const string ruta, const string params){
    FILE *fp = NULL;
    if(ruta && params){
        fp = fopen(ruta, params);
        check_pointer_msg("Couldn't open file", fp);
    }
    return fp;
}

bool close_file(FILE **fp){
    if(fp && *fp){
        if(!fclose(*fp)){ //Si se pudo cerrar correctamente
            *fp = NULL;
            return true;
        }else{
            fprintf(stderr, "Error: Couldn't close file\n");
        }
    }
    return false;
}

bool exist_file(string fileName){
    FILE* fp = NULL;
    fp = fopen(fileName, "r");
    if(fp){
        close_file(&fp);
        return true;
    }
    return false;
}

///////////////////////// NEW ////////////////////////////


void *new_memory(size_t len, size_t tamElem){
    register int i;
    void *ptr = NULL;
    for(i=0; !ptr && i<MAX_INT_R_MEM; i++){
        ptr = calloc(len, tamElem);
    }
    check_pointer_msg("Couldn't allocate memory", ptr);
    return ptr;
}

void *new_memory_copy(const void *ptr, size_t len, size_t tamElem){
    u_int *newPtr = NULL;
    if(ptr && len>0 && tamElem>0){
        newPtr = new_memory(len, tamElem);
        if(newPtr) memcpy(newPtr, ptr, len*tamElem);
    }
    return newPtr;
}

Array *new_Array(size_t len, size_t tamElem){
    Array *array = NULL;
    void *ptr = NULL;
    ptr = new_memory(len, tamElem);
    if(ptr){
        array = (Array *)new_memory(1, sizeof(Array));
        if(array){
            array->ptr = ptr;
            array->len = len;
        }else{
            free_memory(&ptr);
        }
    }
    return array;
}

Array *new_Array_copy(const Array *array, size_t tamElem){
    Array *newA = NULL;
    if(check_Array(array)){
        newA = new_Array(array->len, tamElem);
        if(newA){
            if(!(newA->ptr = new_memory_copy(array->ptr, array->len, tamElem))){
                free_Array(&newA);
            }
        }
    }
    return newA;
}

string new_string_copy(const string un_str){
    string str = NULL;
    size_t l = 0;
    if(un_str){
        l = strlen(un_str);
        str = (string)new_memory_copy(un_str, l + 1, sizeof(char));
    }
    return str;
}


///////////////////////// RESET /////////////////////////////


bool reset_memory(void *ptr, size_t len, size_t tamElem){
    if(check_pointer_msg("Couldn't reset memory, ptr==NULL", ptr) && len>0 && tamElem>0){
        bzero(ptr, len*tamElem);
        return true;
    }
    return false;
}

bool reset_Array(Array *array, size_t tamElem){
    if(check_Array(array)){
        return reset_memory(array->ptr, array->len, tamElem);
    }
    return false;
}


///////////////////////// PRINT /////////////////////////////


bool print_array_int(const int *ptr, size_t len){
    register int i;
    if(ptr && len){
        for(i=0; i<len; i++){
            printf("%d ", *ptr++);
        }
        puts("");
        return true;
    }else{
        puts("null");
        return false;
    }
}

bool print_array_byte(const byte *array, size_t len){
    register int i;
    if(array && len){
        for(i=0; i<len; i++){
            printf("%d ", *array++);
        }
        puts("");
        return true;
    }else{
        puts("null");
        return false;
    }
}

bool print_Array_int(const Array_int *array){
    if(check_Array((Array *)array)){
        printf("[Array: int  Longitud:%u]\n{\n", array->len);
        print_array_int(array->ptr, array->len);
        puts("}");
    }else{
        puts("null");
    }
    return false;
}

bool print_Array_byte(const Array_byte *array){
    if(check_Array((Array *)array)){
        printf("[Array: byte   Longitud:%u]\n{\n", array->len);
        print_array_byte(array->ptr, array->len);
        puts("}");
    }else{
        puts("null");
    }
    return false;
}


///////////////////////// FREE //////////////////////////////


bool free_memory(void **ptr){
    if(ptr && *ptr){
        free(*ptr);
        *ptr = NULL;
        return true;
    }
    return false;
}

bool free_Array(Array **array){
    if(array && *array){
        return (free_memory((void **)&((*array)->ptr)) && free_memory((void **)array));
    }
    return false;
}

//Libera un arreglo que representa espacios de memoria reservada
bool free_Array_memory(Array_memory **memory){
    register int i;
    if(check_pointer(memory) && check_Array((Array *)*memory)){
        for(i=0; i<(*memory)->len; i++){
            free_memory((void **) (((*memory)->ptr)[i]) ); //Liberando cada posicion de memoria
        }
        free_Array((Array **)memory); //Liberando el arreglo en si
        return true;
    }
    return false;
}

////////////////////////// OTRAS /////////////////////


string concat_string(string cad1, string cad2){
    string salida = NULL;
    if(cad1 && cad2){
        salida = (string)new_memory(strlen(cad1) + strlen(cad2) + 1, sizeof(char));
        if(salida){
            strcpy(salida, cad1);
            strcat(salida, cad2);
        }
    }
    return salida;
}