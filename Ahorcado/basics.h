#ifndef BASICS_H
#define BASICS_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "defs.h"
#include "structs.h"

//////////////// Manejo DE CADENAS /////////////
bool chomp(string); //Elimina el ultimo caracter si es enter
/////////////// PARA ENTRADA DE DATOS DEL USUARIO ///////////////
bool clear_input(void);
void clear_screen(void);
bool read_input_to_string(string, size_t);
string read_input(void);

#endif