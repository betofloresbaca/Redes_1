#include "basics.h"


//////////////// MANEJO DE CADENAS ////////////////


//Elimina el ultimo caracter si es enter
bool chomp(string cad){
	int length = 0;
	if(cad){
		length = strlen(cad);
		if(length && *(cad+length-1)=='\n'){
			*(cad+length-1) = '\0';
			return true;
		}
	}
	return false;
}


//////////////////// PARA ENTRADA DE DATOS //////////////////


//Protege el programa de entradas como: 12 34 32
bool clear_input(void){
	string trash = NULL;
	trash = new_memory(MAX_USER_INPUT,sizeof(char));
	if(trash){
		fgets(trash,MAX_USER_INPUT,stdin);
		free_memory((void **)&trash);
		return true;
	}
	return false;
}

//Limpia la pantalla de la terminal en terminales ANSI
void clear_screen(void){
	printf("\33[2J\33[1;1H");
}

bool read_input_to_string(string str, size_t len){
	if(check_pointer(str)){
		reset_memory(str, len, sizeof(char));
		if(fgets(str, len, stdin)){
			chomp(str);
			return true;
		}
	}
	return false;
}

string read_input(void){
	string str = new_memory(MAX_USER_INPUT, sizeof(char));
	if(str && !read_input_to_string(str,MAX_USER_INPUT)){
		free_memory((void **)&str);
	}
	return str;
}