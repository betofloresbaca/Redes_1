#ifndef NETUTILS_H
#define NETUTILS_H

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "defs.h"
#include "structs.h"
#include "basics.h"

#define MAX_SEGMENT_DATA_LEN 1024
#define SERVER_DEFAULT_PORT 4950 //Puerto por defecto del servidor
#define COMMAND_SIZE 15 //Tamanho del commando
#define PARAM_SIZE  30 //Tamanho de un parametro

typedef struct sockaddr Address;
typedef struct sockaddr_in Address_in;
typedef struct Segment Segment;

struct Segment{
    uint32_t num;   //Numero de secuencia
    byte type;      //Tipo
    byte subtype;   //Subtipo
    uint32_t extra; //For extra data
    uint32_t crc;   //Para guardar el CRC del paquete
    uint32_t len;   //Longitud de los datos
    byte data[MAX_SEGMENT_DATA_LEN]; //Datos
};
/* En Segment los tipos son:
     0  Datos juego
          0 Probar letra
          1 Adivinar palabra
     1  Datos de chat
     2  Segmento de control (Sin contenido)
          0  ACK
          1  NACK
 */

int new_UDP_socket(void);
Address_in *new_Address_in_UDP(unsigned long, unsigned short);
bool bind_socket(int, Address_in *);
int send_buffer(int, Address_in *, byte *, size_t);
int recv_buffer(int, Address_in *, byte *, size_t);
bool fill_text_Segment(Segment *, uint32_t, string);
bool fill_bin_Segment(Segment *, uint32_t, byte *, uint32_t);
uint32_t crc32(byte *, size_t);

#endif