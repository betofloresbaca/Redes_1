#include "netutils.h"

//Crea un nuevo socket UDP
int new_UDP_socket(void){
    int sockfd = -1;
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        fprintf(stderr, "Error: couldn't create new UDP socket\n");
    }
    return sockfd;
}

//Crea una estructura Address_in con los daos proporcionados
Address_in *new_Address_in_UDP(unsigned long ip, unsigned short port){
    Address_in *addr = new_memory(1, sizeof(Address_in));
    if(addr){
        addr->sin_family = AF_INET;
        addr->sin_port = htons(port);
        addr->sin_addr.s_addr = ip;
        reset_memory(&(addr->sin_zero), 1, 8);
    }
    return addr;
}

//Establece la direccion de un socket
bool bind_socket(int sockfd, Address_in *addr){
    if(bind(sockfd, (Address *)addr, sizeof(Address)) == -1){
        fprintf(stderr, "Error: Couldn't bind socket %d\n",sockfd);
        return false;
    }
    return true;
}

//Envia un buffer, hace manejo de errores
int send_buffer(int sockfd, Address_in *addr, byte *data, size_t len){
    int numbytes = -1;
    if(check_pointer(addr) && check_pointer(data)){
        while(numbytes == -1){
            numbytes = sendto(sockfd, data, len, 0, (Address *)addr, sizeof(Address));
        }
    }
    return numbytes;
}

//Recibe en un buffer
int recv_buffer(int sockfd, Address_in *addr, byte *data, size_t len){
    int numbytes = -1;
    size_t addrlen = sizeof(Address_in);
    if(check_pointer(addr) && check_pointer(data)){
        reset_memory(data, len, sizeof(byte));
        while(numbytes == -1){
            numbytes = recvfrom(sockfd, data, len, 0, (Address *)addr, (socklen_t *)&addrlen);
        }
    }
    return numbytes;
}

//rellena un segmento de datos
bool fill_text_Segment(Segment *seg, uint32_t num, string text){
    if(check_pointer_msg("Segment==NULL in fill_text_Segment",seg)){
        if(reset_memory(seg, 1, sizeof(Segment))){
            seg->num = num;
            seg->type = 1;
            if(text){
                seg->len = (uint32_t)strlen(text);
                memcpy(seg->data, text, seg->len);
            }
            return true;
        }
    }
    return false;
}

bool fill_bin_Segment(Segment *seg, uint32_t num, byte *data, uint32_t datalen){
    if(check_pointer_msg("Segment==NULL in fill_bin_Segment",seg)){
        if(reset_memory(seg, 1, sizeof(Segment))){
            seg->num = num;
            seg->type = 1;
            if(data){
                seg->len = datalen;
                memcpy(seg->data, data, seg->len);
            }
            return true;
        }
    }
    return false;
}

uint32_t crc32(byte *buf, size_t len){
    uint32_t table[256]; //Tabla para el calculo del crc
    uint32_t rem; //Residuo de cada divicion
    byte octet;
    uint32_t crc = 0; //Donde guardar el valor del crc calculado
    int i, j; //Contadores para los ciclos
    byte *p, *q;
    //Calculando la tabla para el crc
    for (i = 0; i < 256; i++) {
        rem = i;  //Resto de la divicion polinomial
        for (j = 0; j < 8; j++) {
            if (rem & 1) { 
                rem >>= 1;
                rem ^= 0xedb88320; //Polinomio usado
            } else{
                rem >>= 1;
            }
        }
        table[i] = rem;
    }
    crc = ~crc; //Poniendo en unos el crc
    q = buf + len;
    //Calculando el crc
    for (p = buf; p < q; p++) {
        octet = *p;
        crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
    }
    return ~crc;
}

/******************************************************************************
 *       Para ARQ Stop & Wait
 *****************************************************************************/

/*Envia un ACK*/
bool send_ack(int sockfd, Address_in *their_addr){
    Segment segA;
    int dataslen,numbytes;
    segA.num = 0;
    segA.type = 2;      
    segA.subtype = 0;
    //segA.extra = id;
    //Calculando la longitud del Segmento
    dataslen = sizeof(Segment) - MAX_SEGMENT_DATA_LEN - \
               sizeof(segA.len) - sizeof(segA.crc) - \
               sizeof(segA.extra);
    //printf("Aresponder %d bytes\n", dataslen);
    //printf("tipo --> %d  subtipo -->  %d  extra --> %d\n", segA.type, segA.subtype, segA.extra);
    if((numbytes = send_buffer(sockfd, their_addr, (byte *)&segA, dataslen)) == -1){
        return false;
    }
    //puts("Se envio el ACK");
    return true;
}

/* Envia un NACK */
bool send_nack(int sockfd, Address_in *their_addr){
    Segment segA;
    int dataslen,numbytes;
    segA.num = 0;
    segA.type = 2;      
    segA.subtype = 1;
    //segA.extra = id;
    //Calculando la longitud del Segmento
    dataslen = sizeof(Segment) - MAX_SEGMENT_DATA_LEN - \
               sizeof(segA.len) - sizeof(segA.crc) - \
               sizeof(segA.extra);
    //printf("Aresponder %d bytes\n", dataslen);
    //printf("tipo --> %d  subtipo -->  %d  extra --> %d\n", segA.type, segA.subtype, segA.extra);
    if((numbytes = send_buffer(sockfd, their_addr, (byte *)&segA, dataslen)) == -1){
        return false;
    }
    //puts("Se envio el NACK");
    return true;
}

//ToDo Falta corregir para recibir desde dentro de esta funcion con recvfrom y a su vez 
//estar contando el tiempo
bool safe_send_Segment(int sockfd, Address_in *their_addr, Segment *seg, uint32_t len){
    int numbytes;
    clock_t endTime = 0;
    if(!tp || !seg) return false;
    //Calculando el crc
    seg->crc = 0;
    seg->crc = crc32((byte *)seg, (size_t)len);
    //Enviando el archivo
    do{
        puts("\nIntentando");
        if((numbytes = send_buffer(tp->sockfd, &(tp->their_addr), (byte *)seg, len)) == -1){ 
            return false;
        }
        //Esperando cierto tiempo
        endTime = (CLOCKS_PER_SEC*1.0/1000)*WAIT_TIME + clock();
        while((clock() < endTime)){ //Dejando correr el tiempo
            if(tp->com != NONE) break; //Si se puso un ACK o un NACK
        }
        printf("tp->com es:%d\n",tp->com);
    }while((tp->com != ACK)); //Solo sale con un ack
    return true;
}